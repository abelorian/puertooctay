<?php
/*

  Template Name: Pagina - Concejo 1

 */
?>



<!DOCTYPE html>

<html lang="en">

    <?php get_header(); ?>

    <body>

        <header>

            <?php get_template_part('template_nav', 'index'); ?>

            <div class="portada secciones">

                <img src="<?php theme_url() ?>/img/portada-concejo.jpg" alt="Plaza">

            </div>

        </header>

        <div class="contenido contenido-concejos">

            <div class="titulo-video titulo-concejal">

                <img src="<?php theme_url() ?>/img/icono-alcalde.png" alt="icono video">

                <h2>ALCALDE Y CONCEJO MUNICIPAL</h2>

            </div>

            <div class="cont-concejos centro-alcalde">

                <img src="<?php theme_url() ?>/img/perfil-alcalde.jpg" alt="Carlos Mancilla Solís">

                <h2>ALCALDE</h2>

                <h3>Sr. Carlos Mancilla Solís</h3>

                <h4>Fono: 642391756</h4>

                <h5><a href="mailto:alcaldia@munipuertoctay.cl"> alcaldia@munipuertoctay.cl</a></h5>

            </div>

            <?php
            $args = array('post_type' => 'post_concejo', 'posts_per_page' => 10, 'order' => 'ASC');
            $loop = new WP_Query($args);
            while ($loop->have_posts()) : $loop->the_post();

                $imagenfull = get_field('imagenconcejo');
                $imagen = $imagenfull['url'];
                ?>
            <div class="cont-concejos">
                <img src="<?php echo $imagen ?>" alt="foto concejal">
                <?php the_content(); ?>
                
            </div>


                <?php
            endwhile;
            ?>

        </div>

        <?php get_footer(); ?>

    </body>
    <script>
        $(document).ready(function () {
            $('#menu-concejo').addClass("active");
        });
    </script>

</html>

