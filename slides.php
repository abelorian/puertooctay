<div id="slider">

    <ul class="bxslider">

        <li><img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider01.jpg" />

            <div class="frase">

                <h1>"Nuestro compromiso, hacerte sentir como en casa"</h1>

            </div>

        </li>

        <li><img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider02.jpg" />

            <div class="frase">

                <h1>"Nuestra calidad y cercanía hace la diferencia"</h1>

            </div>

        </li>

        <li><img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider03.jpg" />

            <div class="frase">

                <h1>"Vive la comodidad de estar cerca de todo"</h1>

            </div>

        </li>

        <li><img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider04.jpg" />

            <div class="frase">

                <h1>"Tú nos inspiras a seguir mejorando"</h1>

            </div>

        </li>

    </ul>

</div>

<div id="slider-cel">

    <ul class="bxslider">

        <li><img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider01-cel.jpg" />

            <div class="frase">

                <h1>"Nuestro compromiso, hacerte sentir como en casa"</h1>

            </div>

        </li>

        <li><img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider02-cel.jpg" />

            <div class="frase">

                <h1>"Nuestra calidad y cercanía hace la diferencia"</h1>

            </div>

        </li>

        <li><img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider03-cel.jpg" />

            <div class="frase">

                <h1>"Vive la comodidad de estar cerca de todo"</h1>

            </div>

        </li>

        <li><img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider04-cel.jpg" />

            <div class="frase">

                <h1>"Tú nos inspiras a seguir mejorando"</h1>

            </div>

        </li>

    </ul>

</div>