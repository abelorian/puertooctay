<!DOCTYPE html>
<html lang="es_ES">
    <?php get_header(); ?>
    <body>
        <header>
            <?php get_template_part('template_nav', 'index'); ?>           
        </header>

        <?php if (have_posts()):while (have_posts()):the_post(); ?>
                <?php the_content(); ?>
                <?php
            endwhile;
        else: php
            ?>
            <?php _e('No post'); ?>
        <?php endif; ?>
        
        <?php get_footer(); ?>
    </body>
</html>

