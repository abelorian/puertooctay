<?php
/*
  Template Name: Pagina - Contacto
 */
?>

<!DOCTYPE html>
<html lang="en">
    <?php get_header(); ?>
    <body>
        <header>
            <?php get_template_part('template_nav', 'index'); ?>
        </header>
        
        <div class="portada secciones">
                <section class="contact-map" id="contact-map">
                    <style type="text/css" scoped>
                        /* Set a size for our map container, the Google Map will take up 100% of this container */
                        #map {
                            width: 100%;
                            height:350px;
                        }
                    </style>
                    <div id="map"></div>       
                </section>
            </div>
        </header>
        <div class="contenido contenido-deptos">
            <div class="titulo-video">
                
                <h2>CONTACTO</h2>
            </div>
            <section id="formulario">
                <div class="formulario">
                    <!-- Aqui va el codigo del formulario -->
                    <form action="<?php theme_url() ?>/php/contacto.php" method="post">
                        <input id="nombre" type="text" name="nombre" placeholder="Tu nombre*" required="" />
                        <input id="email" type="email" name="email" placeholder="ejemplo@correo.com*" required="" />
                        <input id="ciudad" type="text" name="ciudad" placeholder="Tu ciudad*" required=""/>
                        <textarea id="mensaje" name="mensaje" placeholder="Tu mensaje*" required=""></textarea>
                        <input id="submit" type="submit" name="submit" value="Enviar" />
                    </form>
                </div>
            </section>
        </div>
      


                    <?php if (have_posts()):while (have_posts()):the_post(); ?>
                            <?php the_content(); ?>
                            <?php
                        endwhile;
                    else: php
                        ?>
                        <?php _e('No post'); ?>
                    <?php endif; ?>

               


        <?php get_footer(); ?>
    </body>
    <!-- Google Maps -->
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
        <script src="<?php theme_url() ?>/js/google-maps.js"></script> 
    <script>
        $(document).ready(function () {
            $('#menu-contacto').addClass("active");
        });
    </script>
</html>
