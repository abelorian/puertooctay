<head>
    <title>Ilustre Municipalidad de Puerto Octay</title> 
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1" />
    <meta name="title" content="Ilustre Municipalidad de Puerto Octay" />
    <meta name="description" content="" /> 
    <meta name="robots" content="nofollow"/>
    <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/estilo.css">
    <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/reset.css">
    <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/fonts.css">
    <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/component.css" />
    <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/default.css" />
    <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/lightbox.css">
    <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/formulario-contacto.css">
    <link rel="stylesheet" type="text/css" media="screen"  href="<?php theme_url() ?>/css/screen.css"/>
    <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/menu.css">
    <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/responsivo.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="<?php theme_url() ?>/js/lightbox.js"></script>
    <script src="<?php theme_url() ?>/js/modernizr.custom.js"></script>
    <link rel="shortcut icon" href="http://www.munipuertoctay.cl/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="http://www.munipuertoctay.cl/favicon.ico" type="image/x-icon" /> 
    <link rel="image_src" href="http://www.munipuertoctay.cl/face.jpg"/>
    <!--[if lt IE 9]>
    <script src="dist/html5shiv.js"></script>
    <![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-39800546-5', 'auto');
  ga('send', 'pageview');

</script>
</head>