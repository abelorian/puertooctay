<?php
/*

  Template Name: Hostal Worker - Noticias

 */
?>

<!DOCTYPE html>

<html lang="es_ES">

    <?php get_header(); ?> 

    <body>

        <header>

            <?php get_template_part('nav', 'index'); ?>

        </header>

        <?php get_template_part('templates/slides_news', 'index'); ?>

        <div id="noticias">

            <div class="titulo">

                <h2>Noticias de Hostal Worker</h2>

            </div>

            <?php $query = new WP_Query(array('post_type' => 'post', 'posts_per_page' => 3, 'paged' => get_query_var('paged')));
            ?>

            <?php if ($query->have_posts()):while ($query->have_posts()):$query->the_post(); ?>       

                    <div class="cont-noticias">

                        <a href="<?php the_permalink(1); ?>"><?php the_post_thumbnail(array(110, 110)); ?></a>

                        <div class="texto-noticia">

                            <h3><a href="<?php the_permalink(1); ?>"><?php the_title(); ?></a></h3>

                            <h4><?php echo excerpt(18); ?></h4>

                        </div>

                    </div>                    

                    <?php
                endwhile;

            else: php
                ?>                
                <?php _e('No post'); ?>

            <?php endif; ?>

            <?php wp_pagenavi(array('query' => $query)); ?>
            <div class="navigation">
            </div>
        </div>

        <?php get_footer(); ?>

    </body>
</html>