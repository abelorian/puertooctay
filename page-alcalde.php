<?php
/*
  Template Name: Pagina - Alcalde
 */
?>

<!DOCTYPE html>
<html lang="en">
    <?php get_header(); ?>
    <body>
        <header>
            <?php get_template_part('template_nav', 'index'); ?>

            <div class="portada secciones">
                <img src="<?php theme_url() ?>/img/portada-alcalde.jpg" alt="Plaza">
            </div>
        </header>
        <div class="contenido contenido-alcalde">
            <div class="titulo-video">
                <img src="<?php theme_url() ?>/img/icono-alcalde.png" alt="icono video">
                <h2>SALUDOS DEL ALCALDE</h2>
            </div>
            <div class="descripcion-video descripcion-texto">
                <h3>

                    <?php if (have_posts()):while (have_posts()):the_post(); ?>
                            <?php the_content(); ?>
                            <?php
                        endwhile;
                    else: php
                        ?>
                        <?php _e('No post'); ?>
                    <?php endif; ?>

                </h3>
                <div class="foto-alcalde">
                    <img src="<?php theme_url() ?>/img/foto-alcalde.jpg">
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
    </body>
    <script>
        $(document).ready(function () {
            $('#menu-alcalde').addClass("active");
        });
    </script>
     <script>
    $(function() {

        var btn_movil = $('#nav-mobile'),
            menu = $('#menu').find('ul');

        // Al dar click agregar/quitar clases que permiten el despliegue del menú
        btn_movil.on('click', function (e) {
            e.preventDefault();

            var el = $(this);

            el.toggleClass('nav-active');
            menu.toggleClass('open-menu');
        })

    });
</script>
</html>
