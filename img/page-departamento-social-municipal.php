<?php
/*
  Template Name: Pagina - Dep - Social Mun
 */
?>

<!DOCTYPE html>
<html lang="en">
    <?php get_header(); ?>
    <body>
        <header>
            <?php get_template_part('template_nav', 'index'); ?>

            <div class="portada secciones">
                <img src="<?php theme_url() ?>/img/portada-social-municipal.jpg" alt="Secretaria Municipal">
            </div>
        </header>
        <div id="situar-cont" class="contenido contenido-deptos">
            <div class="titulo-video titulo-deptos">
                <img src="<?php theme_url() ?>/img/icono-alcalde.png" alt="icono video">
                <h1>JEFES DE DEPARTAMENTOS MUNICIPALES</h1>
            </div>

            <?php get_template_part('template_section_deptos', 'index'); ?>

            <div class="seccion-deptos info-depto">
                <!-- perfil de jefe del departamento -->
                <div class="perfil-jefe-depto derecha">
                    <img src="<?php theme_url() ?>/img/perfil-favian.jpg" alt="IIdefonso Favian Guzman Baschamnn">
                    <h2>DEPARTAMENTO SOCIAL MUNICIPAL</h2>
                    <h3>IIdefonso Favian Guzman Baschamnn</h3>
                    <h3>Fono contacto: 642391866</h3>
                    <h3 class="sacar-fondo-verde"><a href="mailto:social@munipuertoctay.cl">Email: social@munipuertoctay.cl</a></h3>
                </div>
                
                <?php if (have_posts()):while (have_posts()):the_post(); ?>
                        <?php the_content(); ?>
                        <?php
                    endwhile;
                else: php
                    ?>
                    <?php _e('No post'); ?>
                <?php endif; ?>
                
            </div>
        </div>
        <?php get_footer(); ?>
    </body>
    <script>
        $(document).ready(function () {
            $('#menu-departamentos').addClass("active");
            $('#depto-social').addClass('active-sub');
        });
    </script>
</html>
