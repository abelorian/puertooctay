<?php
/*

  Template Name: Pagina - Departamentos

 */
?>

<!DOCTYPE html>

<html lang="en">

<?php get_header(); ?>

    <body class="fondo-index">

        <header>

<?php get_template_part('template_nav', 'index'); ?>

            <div class="portada secciones">
                <img src="<?php theme_url() ?>/img/portada-concejo.jpg" alt="Secretaria Municipal">
            </div>
        </header>

        <div class="contenido contenido-deptos">

            <div class="titulo-video titulo-deptos">

                <img src="<?php theme_url() ?>/img/icono-alcalde.png" alt="icono video">

                <h1>JEFES DE DEPARTAMENTOS MUNICIPALES</h1>

            </div>            

<?php get_template_part('template_section_deptos', 'index' ); ?>

            <div class="seccion-deptos info-depto">

                <!-- perfil de jefe del departamento -->

                <div class="perfil-jefe-depto derecha">

                    <img src="<?php theme_url() ?>/img/perfil-juan-escalona.jpg" alt="Juan Alberto Escalona Torres">

                    <h2>SECRETARIO MUNICIPAL</h2>

                    <h3>Sr. Juan Alberto Escalona Torres</h3>

                    <h3>Licenciado en Ciencias del Desarrollo</h3>

                    <h3>Fono contacto: 64-2391758</h3>

                    <h3 class="sacar-fondo-verde"><a href="mailto:secmunicipal@munipuertoctay.cl">secmunicipal@munipuertoctay.cl</a></h3>

                </div>

                <h2>Secretaria Municipal</h2>

                <h3>Las funciones de la Secretaría Municipal están claramente definidas en la Ley Orgánica Constitucional de Municipalidades, correspondiéndole:<br><br>

                    a)  “Dirigir las actividades de secretaría administrativa del alcalde y del Concejo; <br><br>

                    b)  Desempeñarse como ministro de fe en todas las actuaciones municipales, y <br><br>

                    c)  Recibir, mantener y tramitar, cuando corresponda, la declaración de intereses establecida por la Ley N° 18.575”.

                    <br><br>

                    En la Ilustre Municipalidad de Puerto Octay, además de las funciones definidas por Ley, al Secretario Municipal le corresponde asumir las funciones de la Dirección de Tránsito y Transporte Público, que para realizarlas cuenta con una funcionaria con dedicación exclusiva en el aspecto administrativo y les corresponde cumplir con lo siguiente:

                    <br><br>

                    a)  Otorgar y renovar licencias para conducir vehículos; <br><br>

                    b)  Determinar el sentido de circulación de vehículos, en coordinación con los organismos de la Administración del Estado competentes;<br><br>

                    c)  Señalizar adecuadamente las vías públicas, y<br><br>

                    d)  En general, aplicar las normas generales sobre tránsito y transporte públicos en la comuna.<br><br>

                    Tránsito Público<br>

                    Fono contacto: 64-2391757<br>

                    Correo electrónico: transito@munipuertoctay.cl
                </h3>
            </div>
        </div>

<?php get_header(); ?>

    </body>
    <script>
        $(document).ready(function () {
            $('#menu-departamentos').addClass("active");
            $('#depto-secretaria').addClass('active-sub');
        });
    </script>
</html>