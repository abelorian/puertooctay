<?php

//-----KIUBO DISEÑO COMUNICACION CREATIVA-------
$boundary = md5(time());
$fecha = date("d-M-y H:i");
$ip = $_SERVER["REMOTE_ADDR"];

$nombre_admin = "Municipalidad de Puerto Octay";
$email_admin = 'abel.orian@gmail.com';
$url_main = 'info@munipuertoctay.cl';
$url_contacto = 'http://www.munipuertoctay.cl/contacto';
$url_header = 'http://www.corridanocturnacap.cl/assets/img/titulo.png';
$url_footer = 'http://www.corridanocturnacap.cl/assets/img/titulo.png';

$nombre = $_POST["nombre"];
$email = $_POST["email"];
$ciudad = $_POST["ciudad"];
$mensaje = $_POST["mensaje"];
$submit = $_POST["submit"];

if (isset($submit)) {

//Enviar correo a admin
    email_1($email_admin, $nombre, $email, $ciudad, $mensaje, $url_main, $url_header, $url_footer, $fecha, $ip, $boundary, $submit);

//Enviar comprobante al usuario.
    email_2($nombre_admin, $email_admin, $nombre, $email, $url_main, $url_header, $url_footer, $fecha, $ip, $boundary, $submit);
}

//Definicion de funciones
function email_1($email_admin, $nombre, $email, $ciudad, $mensaje, $url_main, $url_header, $url_footer, $fecha, $ip, $boundary, $submit) {

    $email_contenido = '<html>
    <head>
        <style type="text/css">
            body {
                margin-left: 0px;
                margin-top: 0px;
                margin-right: 0px;
                margin-bottom: 0px;
                font-size: 14px;
                text-align: left;
            }
            body p {
                text-align:left;
            }
            b {
                font-weight: bold;
                font-size: 16px;
                text-align:left;
            }
            td {
                text-align: left;
            }
            .registro {
                text-align: left;
                font-style: italic;
            }
            .negrita {
                font-weight: bold;
                text-align:right;
                font-style: italic;
            }
            .centro {
                text-align: left;
            }

        </style>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body link="/* 666666" vlink="/* 999999" alink="/* CCCCCC">
        <table width="700" height="320" border="0" align="center">
            <tr>
                <td width="700" height="141" align="left" valign="top"> 
                    <table width="700" height="141" border="0">
                        <tr>
                            <td width="700" height="141"><a href="' . $url_main . '">
                            <img src="' . $url_header . '" width="700" height="171"></a></td>         
                        </tr>
                    </table> 
                    <table width="700" height="165" border="0" align="left" cellspacing="0">
                        <tr>
                            <td width="336" height="19" class="negrita"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Nombre:</font></td>
                            <td width="354" height="-4" class="registro"><font face="Verdana, Arial, Helvetica, sans-serif"><font size="2">' . $nombre . '</font></font></font></td>
                        </tr>
                        <tr>
                            <td height="14" class="negrita"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Email:</font></td>
                            <td width="354" height="14" class="registro"><font face="Verdana, Arial, Helvetica, sans-serif"><font size="2">' . $email . '</font></font></font></td>
                        </tr>
                        <tr>
                            <td width="336" height="19" class="negrita"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Ciudad:</font></td>
                            <td width="354" height="-4" class="registro"><font face="Verdana, Arial, Helvetica, sans-serif"><font size="2">' . $ciudad . '</font></font></font></td>
                        </tr>';


    if (isset($mensaje)) {
        $email_contenido .='
                            <tr>
                                <td width="336" height="19" class="negrita"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Mensaje:</font></td>
                                <td width="354" height="-4" class="registro"><font face="Verdana, Arial, Helvetica, sans-serif"><font size="2">' . $mensaje . '</font></font></font></td>
                            </tr>';
    }

    $email_contenido .=
            '<tr>
                            <td height="7" class="negrita"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Fecha:</font></td>
                            <td height="7" class="registro"><font face="Verdana, Arial, Helvetica, sans-serif"><font size="2">' . $fecha . '</font></font></font></td>
                        </tr>';

    $email_contenido .=
            '<tr>
                            <td height="14" class="negrita"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">IP:</font></td>
                            <td width="354" height="14" class="registro"><font face="Verdana, Arial, Helvetica, sans-serif"><font size="2">' . $ip . '</font></font></font></td>
                        </tr>';

    $email_contenido .='
                        <tr>
                            <td height="14" class="negrita"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Nº de Registro:</font></td>
                            <td width="354" height="14" class="registro"><font face="Verdana, Arial, Helvetica, sans-serif"><font size="2">' . $boundary . '</font></font></font></td>
                        </tr>';
    $email_contenido .='
                        <tr>
                            <td height="21" colspan="2" class="registro">&nbsp;</td>
                        </tr>
                        <tr>
                        </tr>
                    </table> 
                    <table width="700" height="99" border="0">
                        <tr>
                            <td width="700" height="99" border="0"><img src="' . $url_footer . '" width="701" height="99"></td>         
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>';


    //esto es para enviar un correo al administrador
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    $headers .= "From:" . $email . "\n";
    $asunto = "Mensaje de: " . $nombre;

    if ($submit) {

        mail($email_admin, $asunto, $email_contenido, $headers);
    }
}

function email_2($nombre_admin, $email_admin, $nombre, $email, $url_main, $url_header, $url_footer, $fecha, $ip, $boundary, $submit) {

    $email_contenido = '<html>
    <head>
        <style type="text/css">
            <!--
            body p {
                text-align:center;
            }
            b {
                font-weight: bold;
                font-size: 13px;
                text-align:center;
            }
            td {
                text-align: center;
            }
            .fecha {
                font-weight: bold;
                font-style: italic;
            }
            .IP {
                font-weight: bold;
                font-style: italic;
            }
            .registro {
                font-weight: bold;
                font-style: italic;
            }
            .negrita {
                font-weight: bold;
                font-size: 10px;
                text-align:center;
                font-style: italic;
            }
            .centro {
                text-align: center;
                font-style: italic;
            }
            -->
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body link="/* 666666" vlink="/* 999999" alink="/* CCCCCC">
        <table width="700" height="320" border="0" align="center">
            <tr>
                <td width="700" height="141" align="center" valign="top"> 
                    <table width="700" height="141" border="0">
                        <tr>
                            <td width="700" height="141"><a href="' . $url_main . '"><img src="' . $url_header . '" width="700" height="171"></a></td>         
                        </tr>
                    </table>
                    <table width="700" height="165" border="0" align="center">
                        <tr>
                            <td height="180" colspan="3"><blockquote class="centro"><span class="centro"><font color="#000" size="3" face="Verdana, Arial, Helvetica, sans-serif"><b>Señor(a): ' . $nombre . '</b></font></span><br>
                                    <br>
                                    <font color="#000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Su 
                                    mensaje ha sido enviado a nuestro correo exitosamente</font><font color="/* 999999" size="2" face="Verdana, Arial, Helvetica, sans-serif">. <br>
                                    <br>
                                    </font><font color="#000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Este 
                                    formulario a sido enviado el <span class="fecha">' . $fecha . '.</span><br>
                                    Desde la IP: <span class="IP">' . $ip . ' </span><br>
                                    Numero de Registro:<span class="registro"> ' . $boundary . '</span></font></blockquote></td>
                        </tr>       
                    </table> 
                    <table width="700" height="99" border="0">
                        <tr>
                            <td width="700" height="99" border="0"><img src="' . $url_footer . '" width="701" height="99"></td>         
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>';

    //esto es para enviar un email al usuario
    $cabeceras = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    $cabeceras .= 'From: ' . $nombre_admin . ' <' . $email_admin . '>' . "\r\n"; //cambiar esta linea con tu nombre y el correo

    if ($submit) {
        if (mail("" . $email . "", "Mensaje recibido", $email_contenido, $cabeceras)) {
            echo '<script language="javascript">
                alert("Mensaje enviado, muchas gracias.");
                window.location.href = "' . $url_main . '";
            </script> ';
        } else {
            echo '<script language="javascript">
                alert("Error de envio, inténtelo nuevamente.");
            </script> ';
        }
    }
}
?>
