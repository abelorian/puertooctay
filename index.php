<!DOCTYPE html>
<html lang="en">
    <?php get_header(); ?>
    <body class="fondo-index">
        <header>
            <?php get_template_part('template_nav', 'index'); ?>
            <div class="portada">
                <div class="wrapper">
                    <div id="ei-slider" class="ei-slider">
                        <ul class="ei-slider-large">
                            <li>
                                <img src="<?php theme_url() ?>/img/01slider.jpg" alt="image01" />
                                <div class="ei-title">
                                    <h2></h2>
                                    <h3></h3>
                                </div>
                            </li>
                            <li>
                                <img src="<?php theme_url() ?>/img/02slider.jpg" alt="image02" />
                                <div class="ei-title">
                                    <h2></h2>
                                    <h3></h3>
                                </div>
                            </li>
                            <li>
                                <img src="<?php theme_url() ?>/img/03slider.jpg" alt="image03" />
                                <div class="ei-title">
                                    <h2></h2>
                                    <h3></h3>
                                </div>
                            </li>
                            <li>
                                <img src="<?php theme_url() ?>/img/04slider.jpg" alt="image04" />
                                <div class="ei-title">
                                    <h2></h2>
                                    <h3></h3>
                                </div>
                            </li>

                        </ul><!-- ei-slider-large -->
                        <ul class="ei-slider-thumbs">
                            <li class="ei-slider-element">Current</li>
                            <li><a href="#">Slide 1</a><img src="<?php theme_url() ?>/img/01slider.jpg" alt="thumb01" /></li>
                            <li><a href="#">Slide 2</a><img src="<?php theme_url() ?>/img/02slider.jpg" alt="thumb02" /></li> 
                            <li><a href="#">Slide 3</a><img src="<?php theme_url() ?>/img/03slider.jpg" alt="thumb02" /></li>  
                            <li><a href="#">Slide 4</a><img src="<?php theme_url() ?>/img/04slider.jpg" alt="thumb02" /></li>                       
                        </ul><!-- ei-slider-thumbs -->
                    </div><!-- ei-slider -->

                </div><!-- wrapper -->
            </div>
        </header>
        <div class="contenido">
            <div class="titulo-video">
                <img src="<?php theme_url() ?>/img/icono-videos.png" alt="icono video">
                <h2>RUTA DE LA NATURALEZA</h2>
                <span>23/09/2014</span>
            </div>
            <div class="descripcion-video">
                <h3>Este vídeo se a hecho a modo de homenaje tanto a los hermosos lugares de las cascadas Chile como también a la música de ambientación del grupo Bordemar, que entrega toda la pasividad que se encuentra en tan hermoso lugar.
                    <br>
                    <a href="http://www.youtube.com/watch?v=Ptc1SZgZHhE&index=1&list=UUND2cPteWXeyXzTA01i152w" target="_blank">Ver más videos</a></h3>
                <div class="video">
                    <iframe width="480" height="270" src="http://www.youtube.com/embed/ghxS_fg1I1E?list=UUND2cPteWXeyXzTA01i152w" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="separacion"></div>
            <div class="noticias-home">

                <?php $query = new WP_Query('posts_per_page=3'); ?>

                <?php if ($query->have_posts()):while ($query->have_posts()):$query->the_post(); ?>

                        <div class="first-new">
                            <?php the_post_thumbnail(array(300, 200), array('alt' => 'foto-noticias', 'class' => 'foto1')); ?>
                            
                            <img class="icono-noticias" src="<?php theme_url() ?>/img/icono-noticias.png">
                            <a href="<?php the_permalink(1); ?>"><h2><?php the_title(); ?></h2></a>
                            <span><?php the_time('d-m-y') ?></span>
                            <h3><?php echo excerpt(25); ?></h3>
<!--                            <div class="social">
                                <img src="<?php theme_url() ?>/img/iconos-social-compartir.png"><span>52 compatido</span>
                                <img src="<?php theme_url() ?>/img/iconos-social-vistas.png"><span>15 visto</span>
                                <img src="<?php theme_url() ?>/img/iconos-social-comentar.png"><span>21 comentarios</span>
                            </div>-->
                        </div>

                        <?php
                    endwhile;

                else: php
                    ?>

                    <p><?php _e('No post'); ?></p>

<?php endif; ?>

                <div class="boton-videos">
                    <a href="noticias/">Ver más noticias</a>
                </div>
            </div>
            <div class="separacion2"></div>
            <div class="intereses">
                <div class="redes-sociales">
                    <h4>Estamos en las Redes Sociales</h4>
                    <div id="fb-root">
                    </div>
                    <script>(function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id))
                                return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&appId=506605566054227&version=v2.0";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));
                    </script>
                    <div class="fb-like facebook" data-href="https://www.facebook.com/munioctay" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false">
                    </div>
                    <a href="https://twitter.com/MunPuertoctay" class="twitter-follow-button" data-show-count="true" data-lang="es" data-show-screen-name="false">Seguir a @MunPuertoctay</a>
                    <script>!function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                            if (!d.getElementById(id)) {
                                js = d.createElement(s);
                                js.id = id;
                                js.src = p + '://platform.twitter.com/widgets.js';
                                fjs.parentNode.insertBefore(js, fjs);
                            }
                        }(document, 'script', 'twitter-wjs');
                    </script>
                </div>
                <div class="sitios">
                    <h4>Sitios de ínteres</h4>
                    <a href="https://www.sem.gob.cl/pago/buscar.php?pago_id=14&inst_id=8989665&setiframe=0"><img src="https://www.sem.gob.cl/banner/14/3.png" border="0" width="176" height="100" /></a>
                </div>
            </div>
        </div>

<?php get_footer(); ?>

        <!--slider -->

        <script type="text/javascript" src="<?php theme_url() ?>/js/jquery.eislideshow.js"></script>
        <script type="text/javascript" src="<?php theme_url() ?>/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript">
                        $(function () {
                            $('#ei-slider').eislideshow({
                                animation: 'center',
                                autoplay: true,
                                slideshow_interval: 3000,
                                titlesFactor: 0
                            });
                        });
        </script>
        <script>
            $(document).ready(function () {
                $('#menu-inicio').addClass("active");
            });
        </script>
        <script>
    $(function() {

        var btn_movil = $('#nav-mobile'),
            menu = $('#menu').find('ul');

        // Al dar click agregar/quitar clases que permiten el despliegue del menú
        btn_movil.on('click', function (e) {
            e.preventDefault();

            var el = $(this);

            el.toggleClass('nav-active');
            menu.toggleClass('open-menu');
        })

    });
</script>
    </body>
</html>
