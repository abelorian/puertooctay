<div id="cabecera"> 
    <div class="barra-top">
        <span>Esperanza 555 Puerto Octay | Email: info@munipuertoctay.cl | <a class="entrar" href="http://www.munipuertoctay.cl/wp-admin" target="_blank"/>Entrar</a></span>
    </div>
    <div class="logo">
        <img src="<?php theme_url() ?>/img/logo-puerto-octay.png" alt="Logo Puerto Octay">
    </div>
    <nav id="menu">
        <a href="#" class="nav-mobile" id="nav-mobile"></a>
        <ul>
            <li><a id="menu-inicio" href="http://munipuertoctay.cl/"/>Inicio </a></li>
            <li><a id="menu-alcalde" href="http://munipuertoctay.cl/alcalde">Alcalde</a></li>
            <li><a id="menu-concejo" href="http://munipuertoctay.cl/concejo">Concejo</a></li>
            <li><a id="menu-departamentos" href="http://munipuertoctay.cl/departamentos/secretaria-municipal">Departamentos</a></li>
            <li><a id="menu-noticias" href="http://munipuertoctay.cl/noticias">Noticias</a></li>
            <li><a id="menu-transparencia" href="http://transparencia.puertoctay.cl/" target="_blank">Transparencia</a></li>
            <li><a id="menu-galeria" href="http://munipuertoctay.cl/galeria">Galeria</a></li>
            <li><a id="menu-contacto" href="http://munipuertoctay.cl/contacto">Contacto</a></li>
        </ul>
    </nav>
</div>