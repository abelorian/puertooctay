<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1" />
        
        <meta name="description" content="" /> 
        <meta name="robots" content="nofollow"/>
        <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/estilo.css">
        <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/reset.css">
        <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/fonts.css">
        <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/component.css" />
        <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/default.css" />
        <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/lightbox.css">
        <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/formulario-contacto.css">
        <link rel="stylesheet" type="text/css" media="screen"  href="<?php theme_url() ?>/css/screen.css"/>
		<link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/menu.css">
    	<link rel="stylesheet" type="text/css" href="<?php theme_url() ?>/css/responsivo.css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
        <script src="<?php theme_url() ?>/js/lightbox.js"></script>
        <script src="<?php theme_url() ?>/js/modernizr.custom.js"></script>
        <link rel="shortcut icon" href="http://www.munipuertoctay.cl/favicon.ico" type="image/x-icon" />
        <link rel="icon" href="http://www.munipuertoctay.cl/favicon.ico" type="image/x-icon" /> 
        <link rel="image_src" href="http://www.munipuertoctay.cl/face.jpg"/>
        <!--[if lt IE 9]>
        <script src="dist/html5shiv.js"></script>
        <![endif]-->
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-39800546-5', 'auto');
            ga('send', 'pageview');

        </script>
    </head>

    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&appId=348997555250354&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <body class="fondo-index">
        <header>
            <?php get_template_part('template_nav', 'index'); ?>
            <div class="portada secciones">
            </div>
        </header>
        <div class="contenido contenido-noticias">
            <div class="titulo-video titulo-deptos">
                <img src="<?php theme_url() ?>/img/icono-noticias.png" alt="icono noticia">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="volver-atras derecha">
                <a href="../noticias/"><img src="<?php theme_url() ?>/img/volver-atras.png"></a>
                <h3>Volver</h3>
            </div>

            <?php if (have_posts()):while (have_posts()):the_post(); ?>

                    <div class="seccion-deptos info-depto info-noticia todo-noticia">
                        <!-- foto de noticia -->
                        <div class="foto-noticia derecha foto-noticia-big">
                            <?php the_post_thumbnail(array(200, 200)); ?>
                        </div>
                        <h3>
                            <?php the_content(); ?>
                        </h3>
                        
                        <title><?php the_title(); ?>  - Ilustre Municipalidad de Puerto Octay</title>
                        <meta name="title" content="<?php the_title(); ?> - Puerto Octay" />
                    </div>
                    <!-- los botones de compartir redes sociales -->
                    <div class="redes-sociales-pg">
                            <div class="fb-like" data-href="<?php the_permalink() ?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true">
                            </div>
                                <a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php the_title(); ?>">Tweet</a>
                                    <script>!function (d, s, id) {
                                         var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                                            if (!d.getElementById(id)) {
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = p + '://platform.twitter.com/widgets.js';
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }
                                        }(document, 'script', 'twitter-wjs');
                                     </script>

                                <div class="fb-comments" data-href="<?php the_permalink() ?>" data-numposts="5" data-colorscheme="light">
                                </div>
                    </div>
                    <?php
                endwhile;

            else: php
                ?>

                <p><?php _e('No post'); ?></p>
       
            <?php endif; ?> 
            </div>
        </div>
        <?php get_footer(); ?>
    </body>
    <script>
        $(document).ready(function () {
            $('#menu-noticias').addClass("active");
        });
    </script>
    <script>
    $(function() {

        var btn_movil = $('#nav-mobile'),
            menu = $('#menu').find('ul');

        // Al dar click agregar/quitar clases que permiten el despliegue del menú
        btn_movil.on('click', function (e) {
            e.preventDefault();

            var el = $(this);

            el.toggleClass('nav-active');
            menu.toggleClass('open-menu');
        })

    });
    </script>
</html>
