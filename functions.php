<?php

define('TEMPPATH', get_bloginfo('stylesheet_directory'));
define('IMAGES', TEMPPATH . "/img/");
define('URL', 'http://www.munipuertoctay.cl');
add_theme_support('post-thumbnails');

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    return $excerpt;
}

function content($limit) {
    $content = explode(' ', get_the_content(), $limit);
    if (count($content) >= $limit) {
        array_pop($content);
        $content = implode(" ", $content) . '...';
    } else {
        $content = implode(" ", $content);
    }
    $content = preg_replace('/\[.+\]/', '', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}

function wp_url(){
    echo URL;
}

function theme_url(){
   echo bloginfo('stylesheet_directory');
}

// Register Custom Post Type
function custom_post_gallery() {

	$labels = array(
		'name'                => _x( 'Galerias', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Imagen', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Imagenes Galeria', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'Todas las imagenes', 'text_domain' ),
		'view_item'           => __( 'View Item', 'text_domain' ),
		'add_new_item'        => __( 'Agregar nueva', 'text_domain' ),
		'add_new'             => __( 'Agregar nueva', 'text_domain' ),
		'edit_item'           => __( 'Edit Item', 'text_domain' ),
		'update_item'         => __( 'Update Item', 'text_domain' ),
		'search_items'        => __( 'Buscar', 'text_domain' ),
		'not_found'           => __( 'No encontrada', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'post_gallery', 'text_domain' ),
		'description'         => __( 'Imagen para galeria', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'post_gallery', $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_post_gallery', 0 );

// Register Custom Post Type
function custom_post_concejo() {

	$labels = array(
		'name'                => _x( 'Consejo', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Concejal', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Concejo', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Items', 'text_domain' ),
		'view_item'           => __( 'View Item', 'text_domain' ),
		'add_new_item'        => __( 'Agregar nuevo concejal', 'text_domain' ),
		'add_new'             => __( 'Agregar nuevo', 'text_domain' ),
		'edit_item'           => __( 'Editar', 'text_domain' ),
		'update_item'         => __( 'Actualizar', 'text_domain' ),
		'search_items'        => __( 'Buscar', 'text_domain' ),
		'not_found'           => __( 'No encontrado', 'text_domain' ),
		'not_found_in_trash'  => __( 'No encontrado en papelera', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'post_concejo', 'text_domain' ),
		'description'         => __( 'Concejo', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'custom-fields', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'post_concejo', $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_post_concejo', 0 );

// Register Custom Post Type
function custom_post_imagen() {

	$labels = array(
		'name'                => _x( 'ImagenInicio', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'ImagenInicio', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'ImagenInicio', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'Todos', 'text_domain' ),
		'view_item'           => __( 'View Item', 'text_domain' ),
		'add_new_item'        => __( 'Agregar nueva imagen', 'text_domain' ),
		'add_new'             => __( 'Agregar nueva', 'text_domain' ),
		'edit_item'           => __( 'Editar', 'text_domain' ),
		'update_item'         => __( 'Actualizar', 'text_domain' ),
		'search_items'        => __( 'Buscar', 'text_domain' ),
		'not_found'           => __( 'No encontrado', 'text_domain' ),
		'not_found_in_trash'  => __( 'No encontrado en papelera', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'post_imagen', 'text_domain' ),
		'description'         => __( 'ImagenInicio', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'custom-fields', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'post_imagen', $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_post_imagen', 0 );


// Register Custom Post Type
function custom_post_alcalde() {

	$labels = array(
		'name'                => _x( 'Alcalde', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Alcalde', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Alcalde', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'Todos', 'text_domain' ),
		'view_item'           => __( 'View Item', 'text_domain' ),
		'add_new_item'        => __( 'Nuevo', 'text_domain' ),
		'add_new'             => __( 'Agregar nuevo', 'text_domain' ),
		'edit_item'           => __( 'Editar', 'text_domain' ),
		'update_item'         => __( 'Actualizar', 'text_domain' ),
		'search_items'        => __( 'Buscar', 'text_domain' ),
		'not_found'           => __( 'No encontrado', 'text_domain' ),
		'not_found_in_trash'  => __( 'No encontrado en papelera', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'post_alcalde', 'text_domain' ),
		'description'         => __( 'Alcalde', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'custom-fields', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'post_alcalde', $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_post_alcalde', 0 );