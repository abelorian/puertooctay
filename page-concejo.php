<?php
/*

  Template Name: Pagina - Concejo 1

 */
?>

<!DOCTYPE html>

<html lang="en">

    <?php get_header(); ?>

    <body>

        <header>

            <?php get_template_part('template_nav', 'index'); ?>

            <div class="portada secciones">

                <img src="<?php theme_url() ?>/img/portada-concejo.jpg" alt="Plaza">

            </div>

        </header>

        <div class="contenido contenido-concejos">

            <div class="titulo-video titulo-concejal">

                <img src="<?php theme_url() ?>/img/icono-alcalde.png" alt="icono video">

                <h2>ALCALDE Y CONCEJO MUNICIPAL</h2>

            </div>
            
            <?php
            $args = array('post_type' => 'post_alcalde', 'posts_per_page' => 10, 'order' => 'ASC');
            $loop = new WP_Query($args);
            while ($loop->have_posts()) : $loop->the_post();

                $imagenfull = get_field('imagenalcalde');
                $imagen = $imagenfull['url'];
                ?>
            
            
            <div class="cont-concejos centro-alcalde">

                <img src="<?php echo $imagen ?>" alt="foto alcalde">
                <?php the_content();?>
                

            </div>

                <?php
            endwhile;
            ?>

            

            <?php
            $args = array('post_type' => 'post_concejo', 'posts_per_page' => 10, 'order' => 'ASC');
            $loop = new WP_Query($args);
            while ($loop->have_posts()) : $loop->the_post();

                $imagenfull = get_field('imagenconcejo');
                $imagen = $imagenfull['url'];
                ?>
            
            
            <div class="cont-concejos">
                <img src="<?php echo $imagen ?>" alt="foto concejal">
                <?php the_content();?>
                
            </div>

                <?php
            endwhile;
            ?>

        </div>

        <?php get_footer(); ?>

    </body>
    <script>
        $(document).ready(function () {
            $('#menu-concejo').addClass("active");
        });
    </script>
    <script>
    $(function() {

        var btn_movil = $('#nav-mobile'),
            menu = $('#menu').find('ul');

        // Al dar click agregar/quitar clases que permiten el despliegue del menú
        btn_movil.on('click', function (e) {
            e.preventDefault();

            var el = $(this);

            el.toggleClass('nav-active');
            menu.toggleClass('open-menu');
        })

    });
</script>
</html>