<?php

/*

  Template Name: Pagina - Noticias

 */

?>





<!DOCTYPE html>

<html lang="en">

    <?php get_header(); ?>



    <body class="fondo-index">

        <header>

            <?php get_template_part('template_nav', 'index'); ?>

            <div class="portada secciones">

            </div>

        </header>

        <div class="contenido contenido-noticias">

            <div class="titulo-video titulo-deptos">

                <img src="<?php theme_url() ?>/img/icono-noticias.png" alt="icono noticia">

                <h1>ÚLTIMAS NOTICIAS</h1>

            </div>



            <?php $query = new WP_Query(array('post_type' => 'post', 'posts_per_page' => 3, 'paged' => get_query_var('paged')));

            ?>



            <?php if ($query->have_posts()):while ($query->have_posts()):$query->the_post(); ?>  





                    <div class="seccion-deptos info-depto info-noticia">

                        <!-- foto de noticia -->

                        <div class="foto-noticia izquierda">

                            <?php the_post_thumbnail(array(200, 200)); ?>

                        </div>

                        <!-- 50 caracteres -->

                        <h2 class="titulo-new-boton izquierda"><a href="<?php the_permalink(1); ?>"><?php the_title(); ?></a></h2>

                        <span>25/09/2014</span>

                        <!-- 45caracteres -->

                        <h3 class="titulo-new-boton all"><a href="<?php the_permalink(1); ?>"><?php echo excerpt(45); ?></a>

                        </h3>

                    </div>







                    <?php

                endwhile;



            else: php

                ?>                

                <?php _e('No post'); ?>



            <?php endif; ?>



            <?php wp_pagenavi(array('query' => $query)); ?>



        </div>

        <?php get_footer(); ?>

    </body>
    <script>
        $(document).ready(function () {
            $('#menu-noticias').addClass("active");
        });
    </script>
    <script>
    $(function() {

        var btn_movil = $('#nav-mobile'),
            menu = $('#menu').find('ul');

        // Al dar click agregar/quitar clases que permiten el despliegue del menú
        btn_movil.on('click', function (e) {
            e.preventDefault();

            var el = $(this);

            el.toggleClass('nav-active');
            menu.toggleClass('open-menu');
        })

    });
</script>

</html>

