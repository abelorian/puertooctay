
<?php
/*
  Template Name: Pagina - Galeria 2
 */
?>

<!DOCTYPE html>
<html lang="en">
    <?php get_header(); ?>
    <body>
        <header>
            <?php get_template_part('template_nav', 'index'); ?>

            <div class="portada secciones">
                <img src="<?php theme_url() ?>/img/portada-concejo.jpg" alt="Plaza">
            </div>
        </header>
        <div class="contenido contenido-deptos">
            <div class="titulo-video titulo-deptos">
                <img src="<?php theme_url() ?>/img/icono-videos.png" alt="icono video">
                <h1>GALERIA DE IMAGENES Y VIDEOS</h1>
            </div>
            <div class="seccion-deptos submenu-galeria">        
                <a class="active-sub" href="#">Imágenes</a>
                <a href="#">Vídeos</a>
            </div>
            <div class="seccion-deptos info-depto contenedor-img">
                <!-- galeria de imagenes -->
                <div class="imageRow">
                    <div class="set">


                        <?php
                        $args = array('post_type' => 'post_gallery', 'posts_per_page' => 10);
                        $loop = new WP_Query($args);
                        while ($loop->have_posts()) : $loop->the_post();

                            $imagenfull = get_field('imagenfull');
                            $imagen = $imagenfull['url'];


                            $miniatura = $imagenfull['url'];
                            ?>

                            <div class="single first">
                                <a href="<?php echo $imagen ?>" rel="lightbox[plants]" title="<?php the_title(); ?>" description="<?php the_content(); ?>">
                                    <img src="<?php echo $miniatura ?>" alt="" style="width: 150px;  height: 150px;"></a>
                            </div>

                            <?php
                        endwhile;
                        ?>

                    </div>
                </div>
            </div>


        </div>
    </div>
<?php get_footer(); ?>
</body>

<script>
        $(document).ready(function () {
            $('#menu-galeria').addClass("active");
        });
    </script>

</html>
