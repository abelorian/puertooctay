<?php
/*
  Template Name: Pagina - Dep - Administracion
 */
?>

<!DOCTYPE html>
<html lang="en">
    <?php get_header(); ?>
    <body>
        <header>
            <?php get_template_part('template_nav', 'index'); ?>

            <div class="portada secciones">
                <img src="<?php theme_url() ?>/img/portada-administracion-finanzas.jpg" alt="Administración y Finanzas">
            </div>
        </header>
        <div id="situar-cont" class="contenido contenido-deptos">
            <div class="titulo-video titulo-deptos">
                <img src="<?php theme_url() ?>/img/icono-alcalde.png" alt="icono video">
                <h1>JEFES DE DEPARTAMENTOS MUNICIPALES</h1>
            </div>

            <?php get_template_part('template_section_deptos', 'index'); ?>

            <div class="seccion-deptos info-depto">
                <!-- perfil de jefe del departamento -->

                <?php
                $imagen_array = get_field('imagenencargado');
                $imagen = $imagen_array['url'];
                $datosdepto = get_field('datosdepto');

                if (have_posts()):while (have_posts()):the_post();?>

                        <div class="perfil-jefe-depto derecha sacar-fondo-verde">
                            <img src="<?php echo $imagen ?>" alt="Manfredo Edmundo Teuber Kahle">
                            <?php echo $datosdepto ?>
                        </div>

                        <?php the_content(); ?>
                        <?php
                    endwhile;
                else: php
                    ?>
    <?php _e('No post'); ?>
<?php endif; ?>

            </div>
        </div>
        <?php get_footer(); ?>
    </body>
    <script>
        $(document).ready(function () {
            $('#menu-departamentos').addClass("active");
            $('#depto-administracion').addClass('active-sub');
        });
    </script>
    <script>
    $(function() {

        var btn_movil = $('#nav-mobile'),
            menu = $('#menu').find('ul');

        // Al dar click agregar/quitar clases que permiten el despliegue del menú
        btn_movil.on('click', function (e) {
            e.preventDefault();

            var el = $(this);

            el.toggleClass('nav-active');
            menu.toggleClass('open-menu');
        })

    });
</script>
</html>
