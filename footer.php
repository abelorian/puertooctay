<footer>
    <div class="cont-footer">
        <div class="logo-footer">
            <img src="<?php theme_url() ?>/img/logo-footer.png" alt="logo-footer">
        </div>
        <div class="menu-footer">
            <a href="/departamentos/secretaria-municipal/">Secretaria Municipal</a>
            <a href="/departamentos/secplan/">SECPLAN</a>
            <a href="/departamentos/administracion-finanzas/">Administración y Finanzas</a>
            <a href="/departamentos/juzgado-de-policia-local/">Juzgado de Policia Local</a>
            <a href="/departamentos/social-municipal/">Social Municipal</a>
            <a href="/departamentos/direccion-de-obras-municipales/">Dirección de Obras Municipales</a>
            <a href="/departamentos/turismo-y-fomento/">Turismo y Fomento</a>
            <a href="/departamentos/daem/">DAEM</a>
            <a href="/departamentos/salud/">Salud</a>
            <a href="http://transparencia.puertoctay.cl/" target="_blank">Transparencia</a>
            <a href="http://www.munipuertoctay.cl/wp-admin/" target="_blank">Entrar</a>
            <a href="http://webmail.munipuertoctay.cl/" target="_blank">Webmail</a>
        </div>
        <div class="inferior">
            <h5 class="copyright">© 2014 Municipalidad de Puerto Octay</h5>
            <h5 class="autor">Proyecto realizado por <a href="http://www.facebook.com/maquinitagrafica" target="_blank"><img src="<?php theme_url() ?>/img/maquinita-firma.png" alt="Maquinita Gráfica"></a> & <a href="http://www.kiubo.cl" target="_blank"><img src="<?php theme_url() ?>/img/kiubo-firma.png" alt="Kiubo"></a></h5>
        </div>
    </div>
</footer>