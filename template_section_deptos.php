<div class="seccion-deptos">        
    <a id="depto-secretaria" href="<?php wp_url()?>/departamentos/secretaria-municipal/#situar-cont">Secretaria Municipal</a>
    <a id="depto-secplan" href="<?php wp_url()?>/departamentos/secplan/#situar-cont">SECPLAN</a>
    <a id="depto-administracion" href="<?php wp_url()?>/departamentos/administracion-finanzas/#situar-cont">Administración y Finanzas</a>
    <a id="depto-juzgado" href="<?php wp_url()?>/departamentos/juzgado-de-policia-local/#situar-cont">Juzgado de Policia Local</a>
    <a id="depto-social" href="<?php wp_url()?>/departamentos/social-municipal/#situar-cont">Social Municipal</a>
    <a id="depto-obras" href="<?php wp_url()?>/departamentos/direccion-de-obras-municipales/#situar-cont">Dirección de Obras Municipales</a>
    <a id="depto-turismo" href="<?php wp_url()?>/departamentos/turismo-y-fomento/#situar-cont">Turismo y Fomento</a>
    <a id="depto-daem" href="<?php wp_url()?>/departamentos/daem/#situar-cont">DAEM</a>
    <a id="depto-salud" href="<?php wp_url()?>/departamentos/salud/#situar-cont">Salud</a>
</div>